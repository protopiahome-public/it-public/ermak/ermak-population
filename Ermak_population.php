<?php

	
/*
Plugin Name: Ermak.Population
Plugin URI: http://wp-ermak.ru/?page_id=135&d_d=58
Description: Virtual population bots for Ermak Metagame
Version: 1.0.4
Date: 28.08.2015
Author: Genagl
Author URI: http://www.genagl.ru
License: GPL2
*/
/*  Copyright 2014  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//���������� ���������
function init_textdomain_ermak_pop() { 
	//if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("ermak_pop", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
} 
// �������� �� �� ������ ���������� ������� 
add_action('plugins_loaded', 'init_textdomain_ermak_pop');



if(!is_plugin_active('Ermak/Ermak.php'))
{
	add_action( 'admin_notices',					'after_install_sticker_ermak_pop_1' , 24);
	return;
}

function after_install_sticker_ermak_pop_1()
{
	echo "
	<div class='updated notice smc_notice is-dismissible1' id='install_smco_notice' style='padding:30px!important; font-size:22px!important; font-family:Ubuntu Condensed, Arial!important;'>
		".
			sprintf(__("%s is not active. Please install %s plugin for success.", "smc"), '<b>Ermak Population</b>', '<b>Ermak</b>') .
		"
		<span class='smc_desmiss_button'>
			<span class='screen-reader-text'>".__("Close")."</span>
		</span>
	</div>
	
	";
}


//Paths
define('ERMAK_POPULATION_URLPATH', WP_PLUGIN_URL.'/Ermak_population/');
define('ERMAK_POPULATION_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('ERMAK_POPULATION','ermak_population');
define('ERMAK_POPULATION_GROUP_TYPE', 'er_population_group');

require_once(ERMAK_POPULATION_REAL_PATH.'class/Ermak_population.class.php');
require_once(ERMAK_POPULATION_REAL_PATH.'class/Ermak_Population_Group.class.php');

//add_action("init", 		'init_ermak_population', 12);
//function init_ermak_population()
{
	$Ermak_Population		= new Ermak_Population();
	Ermak_Population_Group::init();
}

register_activation_hook( __FILE__,  'init_pop_settings'  );

function init_pop_settings()
{
	if( is_plugin_active('Ermak/Ermak.php') )		
	{			
		global $wpdb;
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "ermak_population` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`location_id` int(10) unsigned NOT NULL,
				`type` int(10) unsigned NOT NULL,
				`count` int(11) NOT NULL,
				PRIMARY KEY (`ID`),
				KEY `location_population_group` (`location_id`,`type`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;"
			);
		
		SMC_Location::add_properties(
			array(
					"population",
					"population_frontal_consume"
				  ), 
			array(
					"BIGINT(20) UNSIGNED ",
					"tinyint(1) UNSIGNED "
				  ),
			array(
					0,
					0
				  )
		);
		/**/		
	}
}
?>